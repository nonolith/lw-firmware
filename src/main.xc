#include <xs1.h>
#include <platform.h>
#include <print.h>
#include <stdint.h>

#include "xud.h"
#include "usb.h"
#include "xud_interrupt_driven.h"

#define XUD_EP_COUNT_OUT   2
#define XUD_EP_COUNT_IN	   2

#define USB_RST_PORT	XS1_PORT_1I

/* Endpoint type tables */
XUD_EpType epTypeTableOut[XUD_EP_COUNT_OUT] = {XUD_EPTYPE_CTL, XUD_EPTYPE_BUL};
XUD_EpType epTypeTableIn[XUD_EP_COUNT_IN] =   {XUD_EPTYPE_CTL, XUD_EPTYPE_BUL};

/* USB Port declarations */
on stdcore[0]: out port p_usb_rst = USB_RST_PORT;
on stdcore[0]: clock	clk	   = XS1_CLKBLK_3;

void Endpoint0( chanend c_ep0_out, chanend c_ep0_in);

#define MAX_BUF 512

#define CMD_GET_1 1
#define CMD_GET_4 4
#define CMD_FLUSH 0xff

XUD_ep c_ep_in;
XUD_ep c_ep_out;
unsigned int bufToDevice[2][MAX_BUF/sizeof(int)+2];
unsigned int bufToHost[2][MAX_BUF/sizeof(int)+2];
int hostLen = 0, devLen[2] = {0,0};
int devRd = 0;
int devCurrent = 0, hostCurrent = 0;
int hostWaiting = 0;
int devWaiting = 0;
int flushIn = 0;

inline int handleInEp(){
	if (hostWaiting && (hostLen == MAX_BUF || flushIn)){
		XUD_provide_IN_buffer(c_ep_in, 0, bufToHost[hostCurrent], hostLen);
		hostCurrent = !hostCurrent;
		if (hostLen < MAX_BUF){
			flushIn = 0;
		}
		hostLen = 0;
		hostWaiting = 0;
	}
}

void handleEndpoints(chanend chan_ep_in, chanend chan_ep_out, chanend vcomToDevice, chanend vcomToHost) {
	timer tmr;
	unsigned t;

	chan serv;

	unsigned char tmp;

	c_ep_in = XUD_Init_Ep(chan_ep_in);
	c_ep_out = XUD_Init_Ep(chan_ep_out);

	// First set handlers on each of the XUD endpoints, then enable interrupts
	// and store the server channel
	XUD_interrupt_OUT(chan_ep_out, c_ep_out);
	XUD_interrupt_IN(chan_ep_in, c_ep_in);
	XUD_interrupt_enable(serv);

	// Now state that we are ready to listen to IN requests.
	outuchar(serv, c_ep_in);

	// And make a buffer available for OUT requests.
	XUD_provide_OUT_buffer(c_ep_out, bufToDevice[!devCurrent]);

	tmr :> t;
	t += 100000;

	while(1) {
		select {
		case inuchar_byref(serv, tmp):
			if (tmp == (c_ep_in & 0xff)) {
				hostWaiting = 1;
				handleInEp();
			} else if (tmp == (c_ep_out & 0xff)) {
				int l = XUD_compute_OUT_length(c_ep_out, bufToDevice[!devCurrent]);
				devLen[!devCurrent] = l;
				if (devLen[devCurrent] == 0) {
					devCurrent = !devCurrent;
					devRd = 0;
					XUD_provide_OUT_buffer(c_ep_out, bufToDevice[!devCurrent]);
				} else {
					devWaiting = 1;
				}
			}
			break;
		case hostLen != MAX_BUF => vcomToHost :> char x:
			(bufToHost[hostCurrent], unsigned char[])[hostLen++] = x;
			handleInEp();
			break;
		case devLen[devCurrent] != 0 => vcomToDevice :> int _:
			vcomToDevice <: (bufToDevice[devCurrent], unsigned char[])[devRd++];
			devLen[devCurrent]--;
			if (devLen[devCurrent] == 0) {
				if (devWaiting) {
					devCurrent = !devCurrent;
					devRd = 0;
					XUD_provide_OUT_buffer(c_ep_out, bufToDevice[!devCurrent]);                    
					devWaiting = 0;
				}
			}
			break;
		case tmr when timerafter(t) :> void:
			t+=100000;
			if (hostWaiting && hostLen!=0){
				flushIn = 1;
				handleInEp();
			}
			break;
		}
	}

}

inline char readByte(chanend c){
	unsigned char tmp;
	c <: 1;
	c :> tmp;
	return tmp;
}

inline uint16_t read16(chanend c){
	unsigned char tmp[2];
	for (int i=0; i<2; i++){
		tmp[i] = readByte(c);
	}
	return tmp[1] << 8 | tmp[0];	
}

inline unsigned readUint(chanend c){
	// TODO: get the buffer thread to send a uint itself
	unsigned char tmp[4];
	for (int i=0; i<4; i++){
		tmp[i] = readByte(c);
	}
	return tmp[3]<<24 | tmp[2]<<16 | tmp[1] << 8 | tmp[0];
}

inline void sendUint(chanend c, unsigned v){
	c <: (unsigned char) (v>>0);
	c <: (unsigned char) (v>>8);
	c <: (unsigned char) (v>>16);
	c <: (unsigned char) (v>>24);	
}

#define BC_SIZE 4096
unsigned char bytecode[BC_SIZE];
port bc_ports[] = {
	on stdcore[0]: XS1_PORT_1L,
	on stdcore[0]: XS1_PORT_1A,
	on stdcore[0]: XS1_PORT_1C,
	on stdcore[0]: XS1_PORT_1D,
	on stdcore[0]: XS1_PORT_1K,
	on stdcore[0]: XS1_PORT_1B,
	on stdcore[0]: XS1_PORT_1M,
	on stdcore[0]: XS1_PORT_1J,

};

#define BC_HALT        0b00000000
#define BC_JUMP_U      0b00000100 
#define BC_WAIT_U      0b00001000
#define BC_WAIT_C      0b00001001
#define BC_WAIT_PIN_UU 0b00100000
#define BC_IN_U        0b10010000
#define BC_OUT_U       0b11010000
#define BC_OUT_C       0b11000000


void bci(chanend c_out, chanend c_in) {
	unsigned size = read16(c_out);
	unsigned ip = 0;
	timer tmr;
	unsigned t;

	for (int i=0; i<size; i++){
		unsigned char tmp = readByte(c_out);
		if (i < BC_SIZE){
			bytecode[i] = tmp;
		}
	}

	tmr :> t;

	while(1) {
		unsigned char insn = bytecode[ip++];
		
		switch (insn & 0xF0){
			case 0: switch(insn) {
				case BC_HALT:
					// TODO: flush buffer
					ip = read16(c_out);
					c_in <: (unsigned char) 0x55;
					tmr :> t;
					break;

				case BC_JUMP_U:
					ip = read16(c_out);
					break;

				case BC_WAIT_U:
					t += readUint(c_out);
					tmr when timerafter(t) :> t;
					break;

				case BC_WAIT_C:
					t += bytecode[ip] | bytecode[ip+1]<<8 | bytecode[ip+2]<<16 | bytecode[ip+3]<<24;
					ip+=4;
					tmr when timerafter(t) :> t;
					break;

				case 0x0f:
					c_in <: (unsigned char) readByte(c_out);
					break;
				}
				break;


			case BC_WAIT_PIN_UU: {
				unsigned ttmp;
				bc_ports[insn&0x7] when pinseq(readByte(c_out)) :> void;
				tmr :> ttmp;
				sendUint(c_in, ttmp-t);
				t = ttmp;
				break;
			}

			case BC_IN_U: {
				unsigned char tmp;
				bc_ports[insn&0x7] :> tmp;
				c_in <: tmp;
				break;
			}

			case BC_OUT_U:
				bc_ports[insn&0x7] <: readByte(c_out);
				break;

			case BC_OUT_C:
				bc_ports[insn&0x7] <: !!(insn&0x8);
				break;

		}

		if (ip > size || ip == 0) return;
	}
}

void userThread(chanend c_out, chanend c_in){
	while (1) bci(c_out, c_in);
}

int main() {
	chan c_ep_out[2], c_ep_in[2], vcom, vcom2;
	par {
		on stdcore[USB_CORE]: XUD_Manager(
			c_ep_out, XUD_EP_COUNT_OUT,
			c_ep_in, XUD_EP_COUNT_IN,
			null, epTypeTableOut, epTypeTableIn,
			p_usb_rst, clk, -1, XUD_SPEED_HS, null); 
		
		on stdcore[USB_CORE]: Endpoint0( c_ep_out[0], c_ep_in[0]);
		on stdcore[USB_CORE]: handleEndpoints(c_ep_in[1], c_ep_out[1], vcom, vcom2);
		on stdcore[USB_CORE]: userThread(vcom, vcom2);
	}

	return 0;
}
