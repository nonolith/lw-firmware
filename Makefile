TARGET = XTAG2

APP_NAME = app_example_usb

XCC_FLAGS = -Wall -O2 -report -fsubword-select -DUSB_CORE=0

USED_MODULES = module_usb_shared module_xud module_xud_interrupt_driven

XMOS_MODULE_PATH = lib
SOURCE_DIRS = src

USB_ID = 9999:ffff

include $(XMOS_MAKE_PATH)/xcommon/module_xcommon/build/Makefile.common

load: all
	(cd bin; xobjdump --split --strip $(APP_NAME).xe)
	./load.py bin/image_n0c0.bin $(USB_ID)

.PHONY: load