#!/usr/bin/python

import usb
import time

XMOS_TEST_VID = 0x9999
XMOS_TEST_PID = 0xffff
XMOS_TEST_EP_IN = 0x81
XMOS_TEST_EP_OUT = 0x01

dev = usb.core.find(idVendor=XMOS_TEST_VID, idProduct=XMOS_TEST_PID)

BC_HALT   = 0b00000000
BC_JUMP_U = 0b00000100
BC_ECHO   = 0b00001111
BC_WAIT_U = 0b00001000

dev.write(XMOS_TEST_EP_OUT, [7, 0, 0, 0,  BC_HALT, BC_ECHO, BC_WAIT_U, BC_ECHO, BC_ECHO, BC_ECHO, BC_JUMP_U])
dev.write(XMOS_TEST_EP_OUT, [1, 0, 0, 0, 0x97, 0, 0, 0, 1, 0x99, 0x98, 0x99, 0, 0, 0, 0])
time.sleep(0.1)
print dev.read(XMOS_TEST_EP_IN, 512, 0, 1000)
time.sleep(0.1)
print dev.read(XMOS_TEST_EP_IN, 512, 0, 1000)
time.sleep(0.5)
print dev.read(XMOS_TEST_EP_IN, 512, 0, 1000)

